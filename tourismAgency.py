class tourismAgency:
    def __init__(self, tAgCode, name, direction):
        self.tAgCode = tAgCode
        self.name = name
        self.direction = direction
        self.driver_list = []
        self.tour_list = []

        agency = {'tAgCode': self.tAgCode, 'name': self.name, 'direction': self.direction,
                  'driver_list': self.driver_list, 'tour_list': self.tour_list}

        '''for key, value in agency.items():
            temp = [key, value]
            agencys_list.append(temp)'''

        agencys_list.append(agency)


    def _str_(self) -> str:
        return f'tAgCode: {self.tAgCode} ---- Nombre: {self.name} ---- Apellido: {self.direction} ---- Lista de conductores: {self.driver_list}'

class person:
    def __init__(self, id, name, lastName, nationality, age, gender):
        self.id = id
        self.name = name
        self.lastName = lastName
        self.nationality = nationality
        self.age = age
        self.gender = gender

        person = {'Nombre': self.name, 'Identificacion': self.id, 'Apellido': self.lastName,
                  'Nacionalidad': self.nationality, 'Edad': self.age, 'Gender': self.gender}

        persons_list.append(person)

    def _str_(self) -> str:
        return f'Identificacion: {self.id} ---- Nombre: {self.name} ---- Apellido: {self.lastName} ---- Nacionalidad: {self.nationality}' \
            f' ---- Edad: {self.age} ---- Genero: {self.gender}'


class drivers(person, tourismAgency):
    def __init__(self, id, name, lastName, nationality, age, gender, driverCode, tAgCode):
        person.__init__(self, id, name, lastName, nationality, age, gender)
        self.driverCode = driverCode
        self.tAgCode = tAgCode

        driver = {'name': self.name, 'id': self.id, 'lastName': self.lastName,
                  'nationality': self.nationality, 'age': self.age, 'gender': self.gender,
                  'driverCode': self.driverCode, 'tAgCode': self.tAgCode}
        drivers_list.append(driver)

    def _str_(self) -> str:
        return f'Identificacion: {self.id} ---- Nombre: {self.name} ---- Apellido: {self.lastName} ---- Nacionalidad: {self.nationality}' \
            f' ---- Edad: {self.age} ---- Genero: {self.gender} ---- Codigo de agencia: {self.tAgCode}'


class turist(person):
    def __init__(self, id, name, lastName, nationality, age, gender, tourCode):
        person.__init__(self, id, name, lastName, nationality, age, gender)
        self.tourCode = tourCode

        turist = {'name': self.name, 'id': self.id, 'lastName': self.lastName,
                  'nationality': self.nationality, 'age': self.age, 'gender': self.gender, 'tourCode': self.tourCode}

        turist_list.append(turist)


class tours:
    def __init__(self, cod, destiny, date, cost, duration, driverCode, tAgCode):
        self.cod = cod
        self.destiny = destiny
        self.date = date
        self.cost = cost
        self.duration = duration
        self.dirverCode = driverCode
        self.tAgCode = tAgCode
        self.turist_list = []

        tour = {'cod': self.cod, 'destiny': self.destiny, 'date': self.date,
                'cost': self.cost, 'duration': self.duration, 'dirverCode': self.dirverCode,
                'tAgCode':self.tAgCode, 'turist_list': self.turist_list}

        tours_list.append(tour)


agencys_list = []
persons_list = []
drivers_list = []
turist_list = []
tours_list = []
agencys_dict = []
persons_dict = []
drivers_dict = []
turist_dict = []
tours_dict= []


tour1 = tours(cod=654, destiny='Cartagena', date='1/10/29', cost=1234234,duration=2, driverCode=123, tAgCode=789)
tour2 = tours(cod=2343, destiny='medellin', date='1/11/19', cost=4567889,duration=5, driverCode=456, tAgCode=456)
tour3 = tours(cod=9875, destiny='santa marta', date='1/8/10', cost=765987,duration=8, driverCode=789, tAgCode=123)

tourismAgency(tAgCode =213,name='javier', direction='calle 32 #8-21')
tourismAgency(tAgCode =14,name='gustavo', direction='call 34A')

turist(id=1, name='ana', lastName='barrios', nationality='argentinio', age=18, gender="F", tourCode=678)
turist(id=2, name='samuel' lastName="Perez", nationality="Mexicano", age=30, gender='M', tourCode=123)
turist(id=3, name='pedro', lastName='morales', nationality='jappones', age=50, gender='M',  tourCode=1234)

drivers(id=5123456, name='andres', lastName='banos', nationality='colombiano', age=31, gender='M', driverCode=1234, tAgCode=321)
drivers(id=543, name='alan', lastName='moreno', nationality='venezolano', age=28, gender='M', driverCode=987, tAgCode=789)
drivers(id=1234, name='sebastian', lastName='mercado', nationality='chileno', age=20, gender='M', driverCode=654, tAgCode=456)



def change_dic_to_list(dic_to_change):
    dictlist = []
    '''#for agIdx, agRow in enumerate(agencys_list):
    for key, value in dic_to_change.items():
        temp = value
        dictlist.append(temp)
    return dictlist'''
    return dic_to_change


def add_list_in_list(father, son, equalf, equals, list_name_to_add):
    for agIdx, agRow in enumerate(father):
        for drIdx, drivRow in enumerate(son):
            if father[agIdx].get(equalf) == son[drIdx].get(equals):
                father[agIdx].get(list_name_to_add).append(change_dic_to_list(son[drIdx]))
    return father




all_in_one = []
agencys_list = add_list_in_list(agencys_list, drivers_list, 'tAgCode', 'tAgCode', 'driver_list')
tours_list = add_list_in_list(tours_list, turist_list, 'cod', 'tourCode', 'turist_list')
agencys_list = add_list_in_list(agencys_list, tours_list, 'tAgCode', 'tAgCode', 'tour_list')

for agIdx, agRow in enumerate(agencys_list):
    all_in_one.append(change_dic_to_list(agencys_list[agIdx]))


#print(agencys_list)

duration =0

for tourIdx, tourRow in enumerate(tours_list):
    if duration < tours_list[tourIdx].get('duration'):
        duration = tours_list[tourIdx].get('duration')
        aux = tours_list[tourIdx]

print('Total de conductores ', len(drivers_list))
print('Total de Turistas ', len(turist_list))
print('Total de personas ', len(persons_list))

print(all_in_one)


#change_dic_to_list(agencys_list)


#print(sorted(all_in_one.iteritems(), key = lambda x : x[1]))


#all_in_one.sort(key=lambda desc: desc[0][1], reverse=True)

#print(all_in_one)

