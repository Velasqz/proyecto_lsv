import copy
board = [
    "8........",
    "..36.....",
    ".7..9.2..",
    ".5...7...",
    "....457..",
    "...1...3.",
    "..1....68",
    "..85...1.",
    ".9....4..",

]
#
# board = [
#     "6..874.1.",
#     "..9.36...",
#     "...19.8..",
#     "7946.....",
#     "..1.894..",
#     "...41..69",
#     ".7..5..9.",
#     ".539.76..",
#     "9.2.61.47",
# ]


def main():
    global board
    for idx, line in enumerate(board):
        board[idx] = list(line)
        #print(list(line))
    solve()
    #print(board)
    printBoard()
    #print(getPosssibilities(2, 2))


def solve():
    global board

    try:
        fillAllObvious()
    except:
        return False

    if isComplete():
        return True
    i, j = 0, 0
    for rowIdx, row in enumerate(board):
        for colIdx, col in enumerate(row):
            if col == ".":
                i, j = rowIdx, colIdx

    possibilities = getPosssibilities(i, j)
    for value in possibilities:
        snapshop = copy.deepcopy(board)

        board[i][j] = value
        result = solve()
        if result == True:
            return True
        else:
            board = copy.deepcopy(snapshop)

    return False

def fillAllObvious():
    global board
    while True:
        somethingChange = False
        for i in range(0, 9):
            for j in range(0, 9):
                possibilities = getPosssibilities(i, j)
                if possibilities == False:
                    continue
                if len(possibilities) == 0:
                    raise RuntimeError("No moves left")
                if len(possibilities) == 1:
                    board[i][j] = possibilities[0]
                    somethingChange = True

        if somethingChange == False:
            return


def getPosssibilities(i, j):
    global board
    if board[i][j] != ".":
        return False

    possibilities = {str(n) for n in range(1, 10)}

    for val in board[i]:
        possibilities -= set(val)

    for idx in range(0, 9):
        possibilities -= set(board[idx][j])

    iStart = (i // 3) * 3
    jStart = (j // 3) * 3

    subboard = board[iStart:iStart+3]
    for idx, row in enumerate(subboard):
        subboard[idx] = row[jStart:jStart+3]

    for row in subboard:
        for col in row:
            possibilities -= set(col)
    return list(possibilities)


def printBoard():
    global board
    for row in range(len(board)):
       print(board[row])


def isComplete():
    for row in board:
        for col in row:
            if(col == "."):
                return False
    return True

main()